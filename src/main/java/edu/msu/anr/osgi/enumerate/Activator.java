package edu.msu.anr.osgi.enumerate;

import com.dotcms.repackage.org.apache.felix.http.api.ExtHttpService;
import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotcms.repackage.org.osgi.framework.ServiceReference;
import com.dotmarketing.osgi.GenericBundleActivator;

public class Activator extends GenericBundleActivator {

    private final String URI = "/enumerate";

    private EnumerateServlet simpleServlet;
    private ExtHttpService httpService;

    @SuppressWarnings ("unchecked")
    public void start ( BundleContext context ) throws Exception {

        //Initializing services...
        initializeServices( context );

        //Service reference to ExtHttpService that will allows to register servlets and filters
        ServiceReference sRef = context.getServiceReference( ExtHttpService.class.getName() );
        if ( sRef != null ) {

            httpService = (ExtHttpService) context.getService( sRef );
            try {
                //Registering a simple test servlet
                simpleServlet = new EnumerateServlet();
                httpService.registerServlet(URI, simpleServlet, null, null );
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

    public void stop ( BundleContext context ) throws Exception {

        //Unregister the servlet
        if ( httpService != null && simpleServlet != null ) {
            httpService.unregisterServlet( simpleServlet );
        }
    }

}