package edu.msu.anr.osgi.enumerate;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.business.Role;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.containers.model.Container;
import com.dotmarketing.portlets.structure.model.Structure;
import com.dotmarketing.portlets.templates.model.Template;
import com.liferay.portal.model.User;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A servlet which outputs lists of all Content Types, Users, Roles, Containers, and Templates in the system.
 */
public class EnumerateServlet extends HttpServlet {

    private static final long serialVersionUID = 42L;

    protected void doGet ( HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse ) throws ServletException, IOException {
        httpServletResponse.setContentType( "text/html" );
        ServletOutputStream out = httpServletResponse.getOutputStream();
        out.println( "<html><body><h1>Enumerate</h1>" );

        out.println("<h2>Content Types</h2><ul>");
        for (Structure contentType : getContentTypes()) {
           out.println("    <li>"+contentType.getName()+"</li>");
        }
        out.println("</ul>");

        out.println("<h2>Users</h2><ul>");
        for (User user : getUsers()) {
            out.println("    <li>"+user.getFullName()+" &lt;"+user.getEmailAddress()+ "&gt;</li>");
        }
        out.println("</ul>");

        out.println("<h2>Roles</h2><ul>");
        for (Role role : getRoles()) {
            out.println("    <li>"+role.getName()+" ("+role.getDBFQN()+")</li>");
        }
        out.println("</ul>");

        out.println("<h2>Containers</h2><ul>");
        for (Container container : getContainers()) {
            out.println("    <li>"+container.getFriendlyName()+" ("+container.getIdentifier()+")</li>");
        }
        out.println("</ul>");

        out.println("<h2>Templates</h2><ul>");
        for (Template template : getTemplates()) {
            out.println("    <li>"+template.getFriendlyName()+" ("+template.getIdentifier()+")</li>");
        }
        out.println("</ul>");


        out.println( "</body></html>" );
        out.close();
    }

    /**
     * @return A list of all Content Types (Structures) in the system.
     */
    private List<Structure> getContentTypes () {
        try {
            User systemUser = APILocator.getUserAPI().getSystemUser();
            return APILocator.getStructureAPI().find(systemUser, false, false);
        } catch (DotDataException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * @return A list of all Users in the system.
     */
    private List<User> getUsers () {
        try {
            return APILocator.getUserAPI().findAllUsers();
        } catch (DotDataException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * @return A list of all Roles in the system.
     */
    private List<Role> getRoles () {
        try {
            return APILocator.getRoleAPI().findAllAssignableRoles(true);
        } catch (DotDataException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * @return A list of all Containers in the system.
     */
    private List<Container> getContainers () {
        try {
            User systemUser = APILocator.getUserAPI().getSystemUser();
            return APILocator.getContainerAPI().findAllContainers(systemUser, false);
        } catch (DotDataException|DotSecurityException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * @return A list of all Templates in the system.
     */
    private List<Template> getTemplates () {
        try {
            User systemUser = APILocator.getUserAPI().getSystemUser();
            return APILocator.getTemplateAPI().findTemplates(systemUser, true, null, null, null, null, null, 0, Integer.MAX_VALUE, "friendlyName");
        } catch (DotDataException|DotSecurityException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}