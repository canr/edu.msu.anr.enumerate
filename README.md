This servlet enumerates all Users, Roles, Content Types (Structures), Containers, and Templates in the system.


# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.

## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework you may upload the plugin .jar through dotCMS' Dynamic Plugins portlet or manually place the plugin in the dotCMS instance's Apache Felix load directory.


### Filesystem
Apache Felix monitors its "load" and "undeploy" directories to install and uninstall plugin .jar files. You can move .jar files on the filesystem to load and unload them from the OSGi framework.
```
> mv "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/edu.msu.anr.osgi.structuralintegrity.service-*.jar" "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/undeploy"
> cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```

# Usage
In order to use this servlet, visit the page '/app/enumerate' on your site.